"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
import json

import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY
app.secret_key = os.urandom(24)
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    #test code to make sure the database is working
    _items = db.brevetdb.find()
    items = [item for item in _items]

    for item in items:
        delete()
        break
        print(item)

    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

def is_duplicate(km):
    _brevets = db.brevetdb.find()
    brevets = [brevet for brevet in _brevets]

    for brevet in brevets:
        if brevet["km"] == km:
            return True
    return False

#Routing added for project 5

@app.route('/_submit')
def submit():
    print("submit works")
    #need to add the new brevet to the database
    km = request.args.get('km', 1000000, type=float)
    app.logger.debug(km)
    ml = request.args.get('ml', 1000000, type=float)
    app.logger.debug(ml)
    location = request.args.get('location', "", type=str)
    app.logger.debug(location)
    open_time = request.args.get('open', "", type=str)
    app.logger.debug(open_time)
    close_time = request.args.get('close', "", type=str)
    app.logger.debug(close_time)

    #now we need add this to the db
    brevet_doc = {
        'km': km,
        'miles': ml,
        'location': location,
        'open_time': open_time,
        'close_time': close_time
    }

    #check to see if the brevet is a duplicate


    if brevet_doc["km"] != 1000000 and brevet_doc["miles"] != 1000000 and open_time != "Invalid date" and close_time != "Invalid date":
        app.logger.debug("insert")
        #check to see if the brevet is a duplicate
        dup = is_duplicate(brevet_doc["km"])
        result = {"error": False}
        if dup:
            result["error"] = True
        else:
            db.brevetdb.insert_one(brevet_doc)
        
        return flask.jsonify(result=result)
    else:
        app.logger.debug("error")
        result = {"error": True}
        return flask.jsonify(result=result)
        #return "error"

    #return flask.render_template("calc.html"), 200




@app.route("/display")
def display():
    print("display route")
    #need to update the code to get all of the brevets to show up
    _brevets = db.brevetdb.find()
    brevets = [brevet for brevet in _brevets]
    return flask.render_template("display.html", brevets=brevets), 200

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    bl = request.args.get('bl', 999, type=int)
    bt = request.args.get('bt', "", type=str)
    print("bl: {}".format(bl))
    print("bt: {}".format(bt))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    valid = True

    try:
        begin_time = arrow.get(bt)
        print("arrow made: {}".format(begin_time))

        app.logger.debug("bl: {}".format(bl))
        app.logger.debug("bt: {}".format(begin_time.isoformat()))

        open_time = acp_times.open_time(km, bl, begin_time.isoformat())
        close_time = acp_times.close_time(km, bl, begin_time.isoformat())

        app.logger.debug("open: {}".format(open_time))
        app.logger.debug("close: {}".format(close_time))
    except:
        app.logger.debug("check: {}".format("hit"))
        valid = False
        app.logger.debug("valid: {}".format(valid))
        app.logger.debug("check2: {}".format("past"))
        open_time = "Invalid Date"
        close_time = "Invalid Date"
    app.logger.debug("test: {}".format(open_time))
    if open_time == "Invalid Date" or close_time == "Invalid Date":
        valid = False
    
    #fix the open_time
    #get the length of the brevet

    result = {"open": open_time, "close": close_time, "valid": valid}
    app.logger.debug("check res: {}".format(result["valid"]))
    return flask.jsonify(result=result)

def delete():
    db.brevetdb.drop()

def get_brevets():
    _brevets = db.brevetdb.find()
    brevets = [brevet for brevet in _brevets] 
    return brevets   

#############

###Project 6 API Section


#Helped function for the get method for the resources
#returns either a json or cv representation based on the function parameters
def retrieve(format, open_time, close_time, top = 0):
    #get all the open and close times
    brevets = get_brevets()
    app.logger.debug(brevets)

    #check if top was not queried
    if top == 0:
        top = len(brevets)
    app.logger.debug("top: {}".format(top))

    #Perform the conversion
    try:
        if format == "csv":
            #display in csv format
            #need to print it in rows basically
            brevet_list = []
            count = 0
            for brevet in brevets:
                brev_to_add = []
                brev_to_add.append(brevet["km"])
                brev_to_add.append(brevet["miles"])
                if open_time:
                    brev_to_add.append(brevet["open_time"])
                if close_time:
                    brev_to_add.append(brevet["close_time"])
                brevet_list.append(brev_to_add)
                count = count + 1
            #check whether or not the top parameter was set, meaning we shouldn't return all
            if top != len(brevets) and top > 0 and top < len(brevets):
                app.logger.debug("entering top")
                new_brev_list = brevet_list
                #perform the sort of the list
                new_brev_list.sort(key=lambda x: x[1])
                app.logger.debug("made it past sort")
                app.logger.debug(new_brev_list)
                app.logger.debug(top)
                app.logger.debug(new_brev_list[top])
                brevs_to_return = new_brev_list[0:top]
                app.logger.debug("made it past index range")
                return brevs_to_return
            return brevet_list
        else:
            #setup the list of dictionaries aka json format
            brevs = []
            count = 0
            for brevet in brevets:
                app.logger.debug("LOOK HERE")
                app.logger.debug(brevet)
                #make a brevet dictionary that will be inserted into the list
                brevet_insert = {}
                brevet_insert["km"] = brevet["km"]
                brevet_insert["ml"] = brevet["miles"]
                brevet_insert["location"] = brevet["location"]
                #check whether open or close times should be excluded
                if open_time:
                    brevet_insert["open"] = brevet["open_time"]
                if close_time:
                    brevet_insert["close"] = brevet["close_time"]
                
                app.logger.debug("about to add")
                app.logger.debug(brevet_insert)
                #add the brevet dictionary to the list
                brevs.append(brevet_insert)
                app.logger.debug("actually added")
                count = count + 1
                app.logger.debug("about to return")
            #do the same top checking as in the csv section
            if top != len(brevets) and top > 0 and top < len(brevets):
                app.logger.debug("entering top json")
                new_brev_list = brevs
                #learned how to do this from https://stackoverflow.com/questions/4174941/how-to-sort-a-list-of-lists-by-a-specific-index-of-the-inner-list
                new_brev_list.sort(key=lambda x: x["km"])
                app.logger.debug("made it past sort")
                app.logger.debug(new_brev_list)
                app.logger.debug(top)
                app.logger.debug(new_brev_list[top])
                brevs_to_return = new_brev_list[0:top]
                app.logger.debug("made it past index range")
                return {"Brevets": brevs_to_return}
            
            app.logger.debug("RIGHT HERE")
            app.logger.debug(brevs)
            #return a dictionary called Brevets that contains a list of brevets
            return {"Brevets": brevs}
             
    except:
        app.logger.debug("ERROR")
        return "error"
    
        


class OpenAndClose(Resource):
    def get(self, format = " "):

        top = 0
        #get the top query parameter
        try:
            app.logger.debug("args: {}".format(request.args.get('top')))
            top = int(request.args.get('top'))
        except:
            app.logger.debug("no args")
        #call the helper function and return it
        return retrieve(format, True, True, top)


class Open(Resource):
    def get(self, format = " "):

        top = 0
        try:
            app.logger.debug("args: {}".format(request.args.get('top')))
            top = int(request.args.get('top'))
        except:
            app.logger.debug("no args")
        return retrieve(format, True, False, top)
        

class Close(Resource):
    def get(self, format = " ", top = 0):

        top = 0
        try:
            app.logger.debug("args: {}".format(request.args.get('top')))
            top = int(request.args.get('top'))
        except:
            app.logger.debug("no args")
        return retrieve(format, False, True, top)
        


#setup the api resources
#add parameters for part 2 
api.add_resource(Open, '/listOpenOnly', '/listOpenOnly/<string:format>')
api.add_resource(Close, '/listCloseOnly', '/listCloseOnly/<string:format>')
api.add_resource(OpenAndClose, '/listAll', '/listAll/<string:format>')


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.debug = True
    #delete()
    print("Opening for global access on port {}".format(CONFIG.PORT))
    print(acp_times.close_time(0, 400, arrow.get("2017-02-01T09:30")))
    #app.run(port=CONFIG.PORT, host="0.0.0.0")
    app.run(host='0.0.0.0', port=80, debug=True)
