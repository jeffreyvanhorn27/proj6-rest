"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""

import arrow
import math

#  Note for CIS 322:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def calculate_time(open_func, distance, section, time_to_work_on):
  max_speeds = {
    "200": 34,
    "400": 32,
    "600": 30,
    "1000": 28
  }

  min_speeds = {
    "60": 20,
    "200": 15,
    "400": 15,
    "600": 15,
    "1000": 11.428
  }

  speed_to_work_with = 0

  if open_func:
    speed_to_work_with = max_speeds[section]
  else:
    speed_to_work_with = min_speeds[section]
  #we can calculate in one step
  in_hours = distance/speed_to_work_with
  #round down to get the whole numbers of hours and subtract it
  hours_whole = math.floor(in_hours)
  in_hours = in_hours - hours_whole
  #convert the left over into minutes
  extra_minutes = in_hours * 60
  print("hours {}".format(hours_whole))
  print("minutes {}".format(extra_minutes))
      #need to check if the extra minutes will put it over the hour
  total_minutes = extra_minutes + (hours_whole*60)
  print("total_minutes: {}".format(total_minutes))
  #round up the extra minutes
  extra_minutes = round(extra_minutes)
  #alter the time
  time_to_worked_on = time_to_work_on.shift(hours=+hours_whole, minutes=+extra_minutes)
  #print(time_to_work_on)
  print("changed {}".format(time_to_work_on))
  return time_to_worked_on  

#Come back to this function to add error handling and how to deal with brevet not long enough
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)
    try:
      if control_dist_km <= (brevet_dist_km * 1.2) and control_dist_km >= 0:

        time_to_return = brevet_start_time

        if control_dist_km == 0:
          time_to_return = brevet_start_time

        if control_dist_km > 0:
          control_to_use = control_dist_km
          if control_dist_km > 200:
            control_to_use = 200
          time_to_return = calculate_time(True, control_to_use, "200", time_to_return)
        
        if control_dist_km > 200:
          control_to_use = control_dist_km - 200
          
          if control_dist_km > 400:
            control_to_use = 200
          time_to_return = calculate_time(True, control_to_use, "400", time_to_return)
        
        if control_dist_km > 400:
          control_to_use = control_dist_km - 400
          
          if control_dist_km > 600:
            control_to_use = 200
          time_to_return = calculate_time(True, control_to_use, "600", time_to_return)

        if control_dist_km > 600:
          control_to_use = control_dist_km - 600
          time_to_return = calculate_time(True, control_to_use, "1000", time_to_return)

        return time_to_return.isoformat()
      else:
        return None
    except:
      return None
    #return time_to_work_on

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)

    time_to_return = brevet_start_time

    #need to check to see if this is the final control in the race
    try:
      if control_dist_km <= (brevet_dist_km * 1.2) and control_dist_km >= 0:
        if control_dist_km >= brevet_dist_km:
          #decide what the time on the control should be
          if brevet_dist_km == 200:
            return brevet_start_time.shift(hours=+13, minutes =+30).isoformat()
          elif brevet_dist_km == 300:
            return brevet_start_time.shift(hours=+20).isoformat()
          elif brevet_dist_km == 400:
            return brevet_start_time.shift(hours=+27).isoformat()
          elif brevet_dist_km == 600:
            return brevet_start_time.shift(hours=+40).isoformat()
          elif brevet_dist_km == 1000:
            return brevet_start_time.shift(hours=+75).isoformat()
          else:
            print("error when over distance")   

        if control_dist_km == 0:
          time_to_return = time_to_return.shift(hours=+1)

        if control_dist_km > 0 and control_dist_km <= 60:
          control_to_use = control_dist_km
          time_to_return = calculate_time(False, control_to_use, "60", time_to_return)
          time_to_return = time_to_return.shift(hours=+1)
          return time_to_return.isoformat()

        if control_dist_km > 0:
          control_to_use = control_dist_km
          if control_dist_km > 200:
            control_to_use = 200
          time_to_return = calculate_time(False, control_to_use, "200", time_to_return)
        
        if control_dist_km > 200:
          control_to_use = control_dist_km - 200
          
          if control_dist_km > 400:
            control_to_use = 200
          time_to_return = calculate_time(False, control_to_use, "400", time_to_return)
        
        if control_dist_km > 400:
          control_to_use = control_dist_km - 400
          
          if control_dist_km > 600:
            control_to_use = 200
          time_to_return = calculate_time(False, control_to_use, "600", time_to_return)

        if control_dist_km > 600:
          control_to_use = control_dist_km - 600
          time_to_return = calculate_time(False, control_to_use, "1000", time_to_return)

        print(time_to_return.isoformat())
        return time_to_return.isoformat()
      
      else:
        return None
    except:
      return None





