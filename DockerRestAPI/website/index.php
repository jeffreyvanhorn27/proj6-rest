<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of brevets</h1>
        <ul>
            <?php

            //function to display json output
            function displayJSON($contents, $display_open, $display_close){
                $obj = json_decode($contents);
                
                $brevets = $obj->Brevets;
                
                echo "Brevets: [<br>";
                foreach($brevets as $brevet){
                    echo "&nbsp;&nbsp;{<br>";
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;km: $brevet->km <br>";
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ml: $brevet->ml <br>";
                    if($display_open){
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;open: $brevet->open <br>";
                    }
                    if($display_close){
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;close: $brevet->close <br>";
                    }
                    echo "&nbsp;&nbsp;},<br>";
                }
                echo "]";
            }

            //function to display CSV output
            function displayCSV($contents){
                echo "Brevets<br>";
                $brevets = $contents;
                //turn the string that is $contents into a array of entries represented as a CSV
                $brev_array = explode("],", $brevets);
                
                //go through the array
                foreach ($brev_array as $brevet) {
                    //fix up some of the left over mess, such as [, from the explode function
                    $brevet = str_ireplace("[[","&nbsp;", $brevet);
                    $brevet = str_ireplace("[","&nbsp;", $brevet);
                    $brevet = str_ireplace("]", "&nbsp;", $brevet);
                    //strip away white space on edges
                    $brevet = trim($brevet, "&nbsp;");
                    echo "$brevet <br>";
                }
                echo "<br>";
            }

            //Make a bunch of calls to the API and display the JSON or CSV output

            $json = file_get_contents('http://laptop-service/listAll');

            $obj = json_decode($json);
            
            echo "<h4>ListAll</h4>";  
            
            displayJSON($json, true, true);
            
            $json = file_get_contents('http://laptop-service/listOpenOnly');

            $obj = json_decode($json);
            
            
            echo "<br><h4>listOpenOnly</h4>";  
            displayJSON($json, true, false);

            $json = file_get_contents('http://laptop-service/listCloseOnly');

            $obj = json_decode($json);
            
            echo "<br><h4>listCloseOnly</h4>";  
            displayJSON($json, false, true);

            $csv = file_get_contents('http://laptop-service/listAll/csv');

            
            echo "<br><h4>listOpenAndClose/csv</h4>";  
            displayCSV($csv);

            $csv = file_get_contents('http://laptop-service/listOpenOnly/csv');

            
            echo "<br><h4>listOpenOnly/csv</h4>";  
            displayCSV($csv);

            $csv = file_get_contents('http://laptop-service/listCloseOnly/csv');

            
            echo "<br><h4>listCloseOnly/csv</h4>";  
            displayCSV($csv);

            $json = file_get_contents('http://laptop-service/listAll/json');

            
            echo "<br><h4>listOpenAndClose/json</h4>";  
            displayJSON($json, true, true);

            $json = file_get_contents('http://laptop-service/listOpenOnly/json');

            
            echo "<br><h4>listOpenOnly/json</h4>";  
            displayJSON($json, true, false);

            $json = file_get_contents('http://laptop-service/listCloseOnly/json');

            
            echo "<br><h4>listCloseOnly/json</h4>";  
            displayJSON($json, false, true);

            $json = file_get_contents('http://laptop-service/listAll/json?top=3');

            
            echo "<br><h4>listAll/json?top=3</h4>";  
            #displayJSON($json, true, true);
            displayJSON($json, true, true);

            $csv = file_get_contents('http://laptop-service/listAll/csv?top=3');

            
            echo "<br><h4>listAll/csv?top=3</h4>";  
            displayCSV($csv);

            $json = file_get_contents('http://laptop-service/listAll/json?top=5');

            echo "<br><h4>listAll/json?top=5</h4>";  
            #displayJSON($json, true, true);
            displayJSON($json, true, true);

            $csv = file_get_contents('http://laptop-service/listAll/csv?top=6');

            
            echo "<br><h4>listAll/csv?top=6</h4>";  
            displayCSV($csv);            

            $json = file_get_contents('http://laptop-service/listAll/json?top=4');

            echo "<br><h4>listAll/json?top=4</h4>";  
            displayJSON($json, true, true);

            
            ?>
        </ul>
    </body>
</html>
